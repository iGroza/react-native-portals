# ⚛️ react-native-portals 

## 🚀 Install

run

`yarn add react-native-portals`

or

`npm i react-native-portals --save`

## 🪄 Usage

```jsx
// 1. Create your portal
import { createPortal } from 'react-native-portals';

export const GlobalPortal = createPortal();

// 2. Place the Host inside any component
<GlobalPortal.Host/>

// 3. Just insert your component inside the Site and it will move to the Host
<GlobalPortal.Site>
    <Text>Aloha World!</Text>
</GlobalPortal.Site>
```