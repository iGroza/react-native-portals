import React, { useState } from 'react';
import { View } from 'react-native';

export interface IReactPortal {
  Host: React.ComponentType;
  Site: React.ComponentType;
  portalId: string;
}

export function createPortal(portalId = uuidv4()): IReactPortal {
  let _setChildren: (c: any) => void;

  return {
    portalId,
    Host() {
      const [children, setChildren] = useState(null);
      _setChildren = setChildren;
      return <View key={portalId} children={children} />;
    },

    Site: class extends React.Component {
      componentDidMount() {
        const { children } = this.props;
        _setChildren?.(children);
      }

      componentWillUnmount() {
        _setChildren?.(null);
      }

      componentDidUpdate() {
        const { children } = this.props;
        _setChildren?.(children);
      }

      render() {
        return null;
      }
    },
  };
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0;
    const v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}
